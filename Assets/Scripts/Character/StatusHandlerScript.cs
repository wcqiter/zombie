﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusHandlerScript : MonoBehaviour {

	public GameObject CharacterName;
	public GameObject CharacterImage;
	public GameObject CharacterType;
	Character OnlineCharacter;

	// Use this for initialization
	void Start () {
		CharacterName = GameObject.Find ("CharacterName");
		CharacterImage = GameObject.Find ("CharacterImage");
		CharacterType = GameObject.Find ("CharacterType");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onCharacterInit(Character ch) {
		OnlineCharacter = ch;
		if (OnlineCharacter) {
			Sprite type = Resources.Load<Sprite> ("Type/" + ch.Type);
			Sprite image = Resources.Load<Sprite> ("Image/" + ch.Name);
			CharacterType.GetComponent<Image> ().sprite = type;
			CharacterImage.GetComponent<Image> ().sprite = image;
			CharacterName.GetComponent<Text> ().text = ch.Name;
		} else {

			CharacterType.GetComponent<Image> ().sprite = null;
			CharacterImage.GetComponent<Image> ().sprite = null;
			CharacterName.GetComponent<Text> ().text = "None";
		}
	}
}
