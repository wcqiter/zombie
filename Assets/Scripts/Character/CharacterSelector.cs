﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelector : MonoBehaviour {

	public void onCharacterSelect(string character) {
		GameObject player = GameObject.Find("Player");
		CharacterEventScript characterEventScript = player.GetComponent<CharacterEventScript>();
		characterEventScript.changeCharacter(character);
	}
}
