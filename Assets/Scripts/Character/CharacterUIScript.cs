﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterUIScript : MonoBehaviour {

	public GameObject characterSelectPanel;
	public GameObject statusPanel;
	public GameObject[] buttons;
	public GameObject reselectBtn;
	public GameObject confirmBtn;

	public string selectedCharacter;
	// Use this for initialization
	void Start () {
		selectedCharacter = "";
	}

	public void onCharacterButtonSelected(GameObject button) {
		
		string suffix = "_btn";
		string originalName = button.name;
		selectedCharacter = originalName.Replace (suffix, "");
		for(int i = 0; i < buttons.Length; i++)
		{
			if (!buttons [i].Equals (button)) {
				buttons [i].SetActive (false);
			} else {
				
			}
		}
		reselectBtn.SetActive (true);
		confirmBtn.SetActive (true);
	}

	public void onReselect() {
		for(int i = 0; i < buttons.Length; i++)
		{
			buttons [i].SetActive (true);
		}
		reselectBtn.SetActive (false);
		confirmBtn.SetActive (false);
	}

	public void onConfirmCharacter() {
		characterSelectPanel.SetActive (false);
		statusPanel.SetActive (true);
		GetComponent<CharacterSelector> ().onCharacterSelect (selectedCharacter);
	}
}
