﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEventScript : MonoBehaviour {
	GameObject[] charactersArray;
	Dictionary<string, GameObject> characters = new Dictionary<string, GameObject> ();
	public Character[] charactersStat;

	public SpriteRenderer sprite;

	public Animator anim;
	private Renderer rend;
	public bool facingRight;
	public string onlineCharacter;
	Character selectedCharacter;

	public float movingSpeed = 0.0f;
	public float vMovingSpeed = 0.1f;
	public float UpperLimit = 0.0f;
	public float LowerLimit = 0.0f;
	int Line;

	// Use this for initialization
	void Awake () {
		sprite = GetComponent<SpriteRenderer> ();
		facingRight = true;
		onlineCharacter = "";
		characters.Add(onlineCharacter, null);
		charactersArray = GameObject.FindGameObjectsWithTag("Player");
		for(int i = 0; i < charactersArray.Length; i++)
		{
			characters.Add(charactersArray [i].name, charactersArray [i]);
		}
		clear ();
	}
	
	// Update is called once per frame
	void Update () {
		float move = Input.GetAxis ("Horizontal");
		float vMove = Input.GetAxis ("Vertical");
		if (onlineCharacter != "") {
			anim = characters [onlineCharacter].GetComponent<Animator> ();
			anim.SetFloat ("Move", Mathf.Abs (move) + Mathf.Abs(vMove));

			if (move > 0 && !facingRight)
				flip ();
			else if (move < 0 && facingRight)
				flip ();
			
			if (Input.GetButton("Fire1")) {
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
				anim.SetBool ("Fire", true);
			}
			else {
				if (move != 0) {
					GetComponent<Rigidbody2D> ().velocity = new Vector2 (Mathf.Abs (move) / (move) * movingSpeed, vMove * vMovingSpeed);
				} else {
					GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, vMove * vMovingSpeed);
				}
				anim.SetBool ("Fire", false);


				if (transform.position.y >= UpperLimit) {
					transform.position = new Vector2 (transform.position.x, UpperLimit);
				} else if (transform.position.y <= LowerLimit) {
					transform.position = new Vector2 (transform.position.x, LowerLimit);
				}
			}

			Line = (int) (((LowerLimit - transform.position.y) / (UpperLimit - LowerLimit) + 1.0f) * 100.0f);
			sprite = characters [onlineCharacter].GetComponent<SpriteRenderer> ();
			if (sprite)
				sprite.sortingOrder = Line;
		}
	}


	public void changeCharacter(string str) {
		clear ();
		onlineCharacter = str;
		if (str != "") {
			anim = characters [onlineCharacter].GetComponent<Animator> ();
			rend = characters [onlineCharacter].GetComponent<Renderer> ();
			rend.enabled = true;
			for (int i = 0; i < charactersStat.Length; i++) {
				if (charactersStat [i].Name == onlineCharacter) {
					selectedCharacter = charactersStat [i];
				}
			}
			Vector3 theScale = characters [onlineCharacter].GetComponent<Transform> ().localScale;
			if ((!facingRight && theScale.x == 1) || (facingRight && theScale.x == -1)) {
				theScale.x *= -1;
				characters [onlineCharacter].GetComponent<Transform> ().localScale = theScale;
			}

			movingSpeed = selectedCharacter.Speed;
			vMovingSpeed = selectedCharacter.Speed;

			setWeapon (selectedCharacter);
			setStatusPanel (selectedCharacter);
			setHealthPanel (selectedCharacter);

			setCamera (facingRight);
		} else {
			selectedCharacter = null;
			movingSpeed = 0;

			setWeapon(new Character());
			setStatusPanel (new Character ());
			setHealthPanel (new Character ());

			setCamera (facingRight);
		}

	}
	
	void flip ()
	{
		facingRight = !facingRight;
		Vector3 theScale = characters[onlineCharacter].GetComponent<Transform> ().localScale;
		theScale.x *= -1;
		characters[onlineCharacter].GetComponent<Transform> ().localScale = theScale;

		setCamera (facingRight); 
	}

	public void clear () {
		for(int i = 0; i < charactersArray.Length; i++)
		{
			anim = charactersArray[i].GetComponent<Animator> ();
			rend = charactersArray[i].GetComponent<Renderer> ();
			rend.enabled = false;
			charactersArray [i].GetComponent<Transform> ().localScale.Set (1,1,1);
		}
	}

	void setWeapon(Character ch) {
		GameObject weapon = GameObject.Find ("Weapon");
		WeaponScript weaponScript = weapon.GetComponent<WeaponScript> ();
		weaponScript.setPara (ch);
	}

	void setStatusPanel(Character ch) {
		GameObject StatusPanel = GameObject.Find("StatusPanel");
		StatusHandlerScript statusHandlerScript = StatusPanel.GetComponent<StatusHandlerScript>();
		statusHandlerScript.onCharacterInit(ch);
	}

	void setHealthPanel(Character ch) {
		GameObject HPPanel = GameObject.Find("HPPanel");
		HealthScript healthScript = HPPanel.GetComponent<HealthScript>();
		healthScript.Init(ch);
	}

	void setCamera(bool facing){
		GameObject camera = GameObject.Find ("MainCamera");
		CameraScript cameraScript = camera.GetComponent<CameraScript> ();
		cameraScript.onFacingChange (facing);
	}
}