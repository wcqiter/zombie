﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour {

	public LayerMask whatToHit;
	public Transform BulletPrefab;
	public Transform FlashPrefab;

	float timeToFire = 0;

	Character ch;

	public GameObject player;

	public float RapidFireTimer = 0.0f;
	public float StartTimer = 0.0f;
	public int BulletCount = 0;

	// Use this for initialization
	void Awake () {
		if (transform == null) {
			Debug.LogError ("Cannot find transform object.");
		}
		player = GameObject.Find("Player");
	}

	// Update is called once per frame
	void Update () {
		if (ch) {
			if (ch.FireRate != 0) {
				float delta = Time.deltaTime;
				RapidFireTimer += delta;
				if ((Input.GetButton ("Fire1"))&& Time.time > timeToFire) {
					StartTimer += delta;
					timeToFire = Time.time + 1 / ch.FireRate;
					if (RapidFireTimer > ch.RapidFireTimeout) {
						RapidFireTimer = 0.0f;
						BulletCount = 0;
					}
					if (StartTimer > ch.StartTimeout) {
						if (ch.RapidFireBulletLimit == 0) {
							Shoot ();
							RapidFireTimer = 0.0f;
						} else {
							if (BulletCount < ch.RapidFireBulletLimit) {
								Shoot ();
								RapidFireTimer = 0.0f;
								BulletCount++;
							}
						}
					}
				}
				if (Input.GetKeyUp (KeyCode.J))
					StartTimer = 0.0f;
			}
		}
	}

	void Shoot() {
		Vector2 targetPos;
		if (player.GetComponent<CharacterEventScript>().facingRight) {
			targetPos = new Vector2 (transform.position.x + 10, transform.position.y);
		} else {
			targetPos = new Vector2 (transform.position.x - 10, transform.position.y);
		}
		Vector2 firePointPos = new Vector2 (transform.position.x, transform.position.y);
		RaycastHit2D hit = Physics2D.Raycast (firePointPos, targetPos, 1000 , whatToHit);
		Effect();

		Debug.DrawLine (firePointPos, targetPos, Color.cyan);
		if (hit.collider != null) {
			
		}
	}

	public void setPara(Character c) {
		ch = c;
	}

	void Effect() {
		Transform newBullet;
		if (player.GetComponent<CharacterEventScript> ().facingRight) {
			newBullet = Instantiate (BulletPrefab, transform.position, transform.rotation) as Transform;
		} else {
			newBullet = Instantiate (BulletPrefab, transform.position, Quaternion.LookRotation(-transform.forward, Vector3.up)) as Transform;
		}
		newBullet.SendMessage ("setPara", ch);
		newBullet.parent = gameObject.transform;

		Transform newFlash;
		Vector2 positionRight = new Vector2 (transform.position.x + ch.GunOffsetX, transform.position.y + ch.GunOffsetY);
		Vector2 positionLeft = new Vector2 (transform.position.x - ch.GunOffsetX, transform.position.y + ch.GunOffsetY);
		if (player.GetComponent<CharacterEventScript> ().facingRight) {
			newFlash = Instantiate (FlashPrefab, positionRight, transform.rotation) as Transform ;
		} else {
			newFlash = Instantiate (FlashPrefab, positionLeft, Quaternion.LookRotation(-transform.forward, Vector3.up)) as Transform;
		}
		newFlash.parent = gameObject.transform;
		float size = Random.Range (0.2f, 0.3f);
		newFlash.localScale = new Vector2 (size, size);
		Destroy (newFlash.gameObject, Time.fixedDeltaTime*3);
	}
}
