﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour {

	public int startingHealth = 100;
	public int currentHealth;

	public Slider slider;
	public Image fill;
	public Text HeathText;

	bool dead;

	// Use this for initialization
	void Awake () {
		Init (new Character());
	}
	
	// Update is called once per frame
	void Update () {
		UpdateHealth ();
	}

	void UpdateHealth() {
		fill.color = Color.Lerp(Color.red, Color.green, (float)currentHealth/startingHealth);
		HeathText.text = currentHealth.ToString () + "/" + startingHealth.ToString ();
	}

	public void TakeDamage(int amount) {
		currentHealth -= amount;

		slider.value = currentHealth;

		if (currentHealth <= 0 && !dead) {
			Death ();
		}
	}

	void Death() {
		dead = true;
	}

	public void Init(Character ch) {
		if (ch) {
			setStartHealth (ch.HP);
		} else {
			setStartHealth (100);
		}
	}

	void setStartHealth(int h) {
		startingHealth = h;
		currentHealth = h;

		slider.maxValue = startingHealth;
		slider.value = currentHealth;

	}
}		
