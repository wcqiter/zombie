﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterHealthScript : MonoBehaviour {

	public Monster monster;

	public int startingHealth;
	public int currentHealth;

	bool dead;

	public Canvas UICanvas;
	public GameObject EnemyHealthPrefab;
	public float HealthPanelOffset = 0.35f;
	GameObject EnemyHealth;
	Slider slider;

	void Start() {
		UICanvas = GameObject.Find ("UICanvas").GetComponent<Canvas>();
		startingHealth = monster.HP;
		currentHealth = monster.HP;

		EnemyHealth = Instantiate (EnemyHealthPrefab) as GameObject;
		EnemyHealth.transform.SetParent (UICanvas.transform, false);

		slider = EnemyHealth.GetComponent<Slider> ();
		slider.maxValue = startingHealth;
		slider.value = startingHealth;
	}
		
	void Update () {
		slider.value = (float)currentHealth / (float)startingHealth * (float)startingHealth;
		Vector3 worldPos = new Vector3 (transform.position.x, transform.position.y + HealthPanelOffset, transform.position.z);
		Vector3 screenPos = Camera.main.WorldToScreenPoint (worldPos);
		EnemyHealth.transform.position = new Vector3 (screenPos.x, screenPos.y, screenPos.z);
	}
	
	public void TakeDamage(int amount) {
		currentHealth -= amount;

		slider.value = currentHealth;

		if (currentHealth <= 0 && !dead) {
			Death ();
		}
	}

	void Death() {
		dead = true;
		Destroy (this.gameObject);
		Destroy (EnemyHealth);
	}
}
