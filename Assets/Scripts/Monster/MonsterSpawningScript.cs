﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawningScript : MonoBehaviour {

	public GameObject monsterToSpawn;
	public GameObject Player;

	float Timer;
	public float SpawnInterval;
	public int SpawnDistance;
	bool firstSpawn;

	// Use this for initialization
	void Start () {
		Timer = 5.0f;
		firstSpawn = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.Find ("Player").GetComponent<CharacterEventScript> ().onlineCharacter != "") {
			Timer += Time.deltaTime;
			if ((Timer > SpawnInterval && SpawnInterval!=0) || !firstSpawn) {
				Timer = 0;
				GameObject enemy = Instantiate (monsterToSpawn) as GameObject;
				enemy.transform.parent = GameObject.Find ("Monster").GetComponent<Transform> ();
				if(GameObject.Find ("Player").GetComponent<CharacterEventScript> ().facingRight)
					enemy.transform.position = new Vector3 (Player.GetComponent<Transform> ().position.x + SpawnDistance, Player.GetComponent<Transform> ().position.y, Player.GetComponent<Transform> ().position.z);
				else
					enemy.transform.position = new Vector3 (Player.GetComponent<Transform> ().position.x - SpawnDistance, Player.GetComponent<Transform> ().position.y, Player.GetComponent<Transform> ().position.z);
				firstSpawn = true;
			}
		}
	}
}
