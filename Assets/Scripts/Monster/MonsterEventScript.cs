﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterEventScript : MonoBehaviour {

	public Monster monster;
	public GameObject Player;
	public float DistanceOffset = 0.01f;

	Vector2 PlayerPos;
	Vector2 MonsterPos;
	Vector2 TargetPos;

	bool facingRight;

	float UpperLimit;
	float LowerLimit;
	int Line;

	bool fire;

	public SpriteRenderer sprite;

	void Awake() {
		Player = GameObject.Find ("Player");
		sprite = GetComponent<SpriteRenderer> ();
		facingRight = false;
		UpperLimit = Player.GetComponent<CharacterEventScript> ().UpperLimit;
		LowerLimit = Player.GetComponent<CharacterEventScript> ().LowerLimit;
	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		PlayerPos = Player.GetComponent<Transform> ().position;
		MonsterPos = transform.position;
		if (facingRight)
			TargetPos = new Vector2 (PlayerPos.x - monster.AttackRange, PlayerPos.y);
		else
			TargetPos = new Vector2 (PlayerPos.x + monster.AttackRange, PlayerPos.y);

		Vector2 TargetMonster = TargetPos - MonsterPos;
		Vector2 PlayerMonster = PlayerPos - MonsterPos;
		if ((TargetPos.x >= MonsterPos.x && MonsterPos.x >= PlayerPos.x)
			|| 
			(PlayerPos.x >= MonsterPos.x && MonsterPos.x >= TargetPos.x)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
			fire = true;
		} else {
			Debug.Log ("Target=" + TargetPos.x.ToString () + " ;Monster=" + MonsterPos.x.ToString () + " ;Player=" + PlayerPos.x.ToString ());

			Vector2 NormalizedDirection = TargetMonster / TargetMonster.magnitude;

			transform.Translate (NormalizedDirection * Time.deltaTime * monster.Speed);

			fire = false;
			
		}

		Vector2 Flip = PlayerPos - MonsterPos;
		if (Flip.x > 0 && !facingRight) {
			Debug.Log ("Target=" + Mathf.Abs (TargetPos.x).ToString () + " ;Monster=" + Mathf.Abs (MonsterPos.x).ToString () + " ;Player=" + Mathf.Abs (PlayerPos.x).ToString ());

			flip ();
		} else if (Flip.x < 0 && facingRight) {
			Debug.Log ("Target=" + Mathf.Abs(TargetPos.x).ToString () + " ;Monster=" + Mathf.Abs(MonsterPos.x).ToString () + " ;Player=" + Mathf.Abs(PlayerPos.x).ToString ());
		
			flip ();
		}
		
		Line = (int) (((LowerLimit - transform.position.y) / (UpperLimit - LowerLimit) + 1.0f) * 100.0f);
		if (sprite)
			sprite.sortingOrder = Line;
	}
		
	void flip ()
	{
		facingRight = !facingRight;
		Vector3 theScale = GetComponent<Transform> ().localScale;
		theScale.x *= -1;
		GetComponent<Transform> ().localScale = theScale;
	}
}
