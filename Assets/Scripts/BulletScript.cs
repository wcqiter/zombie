﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	public GameObject Player;
	public int moveSpeed;
	public float rangeDivisionFactor = 33;
	float destroyTimeout;

	Character ch;

	public SpriteRenderer sprite;

	float UpperLimit;
	float LowerLimit;
	int Line;

	void Awake () {
		Player = GameObject.Find ("Player");
		sprite = GetComponent<SpriteRenderer> ();
		UpperLimit = Player.GetComponent<CharacterEventScript> ().UpperLimit;
		LowerLimit = Player.GetComponent<CharacterEventScript> ().LowerLimit;
	}
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * Time.deltaTime * moveSpeed);

		Line = (int) (((LowerLimit - transform.position.y) / (UpperLimit - LowerLimit) + 1.0f) * 100.0f);
		if (sprite)
			sprite.sortingOrder = Line;
		
		Destroy (this.gameObject, destroyTimeout);
	}

	public void setPara(Character c) {
		ch = c;
		destroyTimeout = (float)ch.Range / (float)moveSpeed / (float)rangeDivisionFactor;
		transform.position = new Vector2 (transform.position.x, transform.position.y + ch.GunOffsetY);
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.tag == "Enemy") {
			other.gameObject.GetComponent<MonsterHealthScript> ().TakeDamage (ch.Damage);
			if (!ch.Penetrate)
				Destroy (this.gameObject);
		}
	}
}
