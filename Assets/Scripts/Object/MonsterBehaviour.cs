﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "MonsterBehaviour")]
public class MonsterBehaviour : ScriptableObject {

	public string Name = "Default";
}
