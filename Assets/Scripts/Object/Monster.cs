﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Monster")]
public class Monster : ScriptableObject {

	public string Name = "Default";
	public int HP = 10;
	public int Damage = 1;
	public float Speed = 1.0f;
	public float AttackRange = 3;
}
