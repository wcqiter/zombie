﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Character")]
public class Character : ScriptableObject {

	public string Name = "Default";
	public string Type = "HG";
	public int HP = 100;
	public int Damage = 10;
	public float Speed = 1.0f;
	public float FireRate = 0;
	public float RapidFireTimeout = 0;
	public float StartTimeout = 0;
	public int RapidFireBulletLimit = 3;
	public int Range = 100;
	public bool Penetrate = false;
	public float GunOffsetY = 0.0f;
	public float GunOffsetX = 0.5f;
}
