﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEventScript : MonoBehaviour {

	public GameObject characterSelectPanel;
	public GameObject statusPanel;

	private GameObject characterSprite;
	public Character[] characters;

	// Use this for initialization
	void Start () {
		characterSelectPanel.SetActive (true);
		statusPanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
