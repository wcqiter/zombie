﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour {
	public GameObject Player;
	public float BackgroundWidth;
	public float BackgroundScale;
	public float width;
	public float BGPosX;
	public float PlayerPosX;
	public float DefaultY;
	public float offset;
	// Use this for initialization
	void Start () {
		width = BackgroundWidth * BackgroundScale / 100.0f;
	}
	
	// Update is called once per frame
	void Update () {
		BGPosX = transform.position.x;
		PlayerPosX = Player.GetComponent<Transform> ().position.x;
		offset = (BGPosX - PlayerPosX);
		if (Mathf.Abs(offset) > width) {
			if (offset > 0) {
				transform.position = new Vector2 (transform.position.x - width, DefaultY);
			} else {
				transform.position = new Vector2 (transform.position.x + width, DefaultY);
			}
		}
		
	}
}
