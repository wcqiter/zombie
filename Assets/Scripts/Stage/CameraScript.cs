﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	public float interpVelocity;
     public float minDistance;
	public float followDistance;
	public bool lockY = false;
	public float defaultY = 0;
     public GameObject target;
     public Vector3 offset;
     Vector3 targetPos;
     // Use this for initialization
     void Start () {
         targetPos = transform.position;
     }
     
     // Update is called once per frame
     void FixedUpdate () {
         if (target)
         {
             Vector3 posNoZ = transform.position;
             posNoZ.z = target.transform.position.z;
  
             Vector3 targetDirection = (target.transform.position - posNoZ);
  
             interpVelocity = targetDirection.magnitude * 5f;
  
             targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime); 
  
             transform.position = Vector3.Lerp( transform.position, targetPos + offset, 0.25f);
  
         }
     }

	void LateUpdate() {
		if (target)
			if (lockY)
				transform.position = new Vector3 (transform.position.x, defaultY + offset.y, transform.position.z);
	}

	public void onFacingChange(bool dir) {
		if (dir)
			setOffsetX (0.25f);
		else
			setOffsetX (-0.25f);
	}

	public void setOffsetX(float num) {
		offset.x = num;
	}
}
